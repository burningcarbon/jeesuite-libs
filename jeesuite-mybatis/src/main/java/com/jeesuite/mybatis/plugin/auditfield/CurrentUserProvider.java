package com.jeesuite.mybatis.plugin.auditfield;

public interface CurrentUserProvider {

	String currentUser();
}
